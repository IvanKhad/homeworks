let head = document.head,
    link = document.createElement('link');
link.rel = 'stylesheet';

if (localStorage.getItem('themeStyle') === 'dark') {
    link.href = 'styles/dark.css';

}

else {
    link.href = 'styles/light.css';
}
head.appendChild(link);


let changeButton = document.getElementById('theme');
let status=false;
changeButton.onclick = function() {

    if(status) {
        link.href = 'styles/dark.css'
        localStorage.setItem('themeStyle', 'dark');


    }else {
        link.href = 'styles/light.css';
        localStorage.setItem('themeStyle', 'light');
        status=true;
    }
};

