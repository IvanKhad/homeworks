const userPrice = document.getElementById("userPrice");
userPrice.style.outline = "none";
/*При фокусе должна появится рамка зелёного цвета*/
userPrice.onfocus = () => {
    userPrice.style.borderColor = "Lime";
};

/*При потере фокуса она пропадает*/
userPrice.onblur = () => {
    if (+userPrice.value >= 0) {
        userPrice.style.borderColor = "";
    }
};

const div = document.createElement("div");
const span = document.createElement("span");
div.appendChild (span); /*в div вставили span*/
const btn = document.createElement("button");
btn.innerText = "x";
/*При нажатии на "Х" кнопка с текстом должны быть удалены*/
btn.onclick = () => {
    div.remove();
};

/*Выводим фразу*/
div.appendChild(btn);
const p =document.createElement("p");
p.innerText= "Enter correct price please";
p.style.color = "red";

/*Если пользователь ввел число меньше 0 - при потере фокуса подсвечивать поле ввода красной рамкой, под полем выводить фразу - Please enter correct price.
span со значением при этом не создается.
 */
userPrice.onchange = () => {
    if (+userPrice.value >= 0) {
        span.innerText = ` Текущая цена: ${+userPrice.value}`;
        document.body.insertBefore(div, document.body.firstChild);/*вставили в body - div перед первым ребенком body*/
        p.remove();
        userPrice.style.color = "green";
    } else {
        userPrice.style.borderColor = "red";
        userPrice.style.color = "";
        document.body.appendChild(p);
        div.remove();
    }
};