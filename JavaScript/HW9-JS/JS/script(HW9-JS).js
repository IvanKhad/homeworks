const tabs =document.getElementById("tabs");
    tabsContent = document.getElementById("tabsContent");

/*При  на 1-ю вкладку не отображается весь текст кроме первой вкладки*/
for (let i = 1; i<tabsContent.children.length; i++) {
    tabsContent.children[i].style.display = "none";
}

/*Показывает при нажатии на вкладку показывает её индекс*/
for ( let i = 0; i<tabs.children.length; i++) {
    tabs.children[i].dataset.index = `${i}`;
}


tabs.onclick = () => {
    for (let i = 0; i<tabs.children.length; i++){
        tabs.children[i].className = "tabs-title";
        tabsContent.children[i].style.display = "none";
    }

    let target= event.target;
     target.className = "tabs-title active";
    tabsContent.children[target.dataset.index].style.display = "block";
};
