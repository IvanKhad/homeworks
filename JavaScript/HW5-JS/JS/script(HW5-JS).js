function createNewUser() {

    const newUser = {
        firstName: prompt("Enter your name", "Ivan"),
        lastName: prompt("Enter your last name", "Khaddad"),
        birthday: prompt("Enter your date of birthday", "01.09.1995"),
        getLogin: function () {
            return this.firstName[0].toLowerCase()+ this.lastName.toLowerCase();
        },
        getAge: function(){
            let dd = this.birthday.slice(0,2);
            let mm = this.birthday.slice(3,5) - 1;
            let yyyy = this.birthday.slice(6,10);
            let dateOfBirthday = new Date(yyyy, mm, dd);
            let age = (new Date() - dateOfBirthday)/31536000000;

            return Math.floor(age);
        },
        getPassword: function(){
            return this.firstName[0].toUpperCase() + this.lastName.toLowerCase()+this.birthday.slice(6,10);
        }
    };
    return newUser;
}


const userIvan = createNewUser();


console.log(userIvan);
console.log("You are "  + userIvan.getAge() + " years old");
console.log("Your recommended password is: " +  userIvan.getPassword());
