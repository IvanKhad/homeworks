function show() {
    var p = document.getElementById('pwd');
    p.setAttribute('type', 'text');
    document.getElementById("eye").classList.remove('fa-eye');
    document.getElementById("eye").classList.add('fa-eye-slash');
}

function hide() {
    var p = document.getElementById('pwd');
    p.setAttribute('type', 'password');

    document.getElementById("eye").classList.remove('fa-eye-slash');
    document.getElementById("eye").classList.add('fa-eye');
}
function showconf() {
    var p = document.getElementById('confpwd');
    p.setAttribute('type', 'text');
    document.getElementById("eye2").classList.remove('fa-eye');
    document.getElementById("eye2").classList.add('fa-eye-slash');
}

function hideconf() {
    var p = document.getElementById('confpwd');
    p.setAttribute('type', 'password');

    document.getElementById("eye2").classList.remove('fa-eye-slash');
    document.getElementById("eye2").classList.add('fa-eye');
}

var pwShown = 0;

document.getElementById("eye").addEventListener("click", function () {
    if (pwShown == 0) {
        pwShown = 1;
        show();
    } else {
        pwShown = 0;
        hide();
    }
}, false);
var pwShown = 0;

document.getElementById("eye2").addEventListener("click", function () {
    if (pwShown == 0) {
        pwShown = 1;
        showconf();
    } else {
        pwShown = 0;
        hideconf();
    }
}, false);
document.getElementById("check").addEventListener("click", function () {

    var confirmpass = document.getElementById('confpwd').value;
    var pass=document.getElementById('pwd').value;
    if (pass != confirmpass)

        document.getElementById('alert').style.display = "block";
    else
        alert('You are welcome');

},false);
